// import User from "../services/authentication/authentification";
import Service  from "../services/service/service"
export default interface AppointmentDetails {
    service:Service,
    appointmentsDates:AppointmentsDates[],
}


export interface AppointmentsDates{
    start:Date,
    end:Date
}

export interface ValidationErrors {
    service: string | null;
    employee:string |  null,
    date: string | null,
    startHour: string | null,
    endHour:string |  null,
  }