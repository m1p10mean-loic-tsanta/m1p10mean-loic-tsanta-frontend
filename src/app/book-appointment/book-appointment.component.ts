import { isPlatformBrowser, NgForOf, NgIf } from '@angular/common';
import { Component, Inject, Input, OnInit, PLATFORM_ID } from '@angular/core';
import {
  FormControl,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import {
  Router,
  RouterLink,
  RouterLinkActive,
  RouterOutlet,
} from '@angular/router';
import { ValidationErrors } from '../book-appointment/book-appointment';
import { CustomDatePickerComponent } from '../custom-date-picker/custom-date-picker.component';
import { AddAppointment } from '../services/appointment/add-appointment';
import { AddAppointmentService } from '../services/appointment/add-appointment.service';
import { SessionService } from '../services/authentication/session.service';
import Employee from '../services/employee/employee';
import { EmployeeService } from '../services/employee/employee.service';
import Service from '../services/service/service';
import { ServiceService } from '../services/service/service.service';
import Appointment from './../services/appointment/add-appointment';
@Component({
  selector: 'app-book-appointement',
  standalone: true,
  imports: [
    NgForOf,
    NgIf,
    ReactiveFormsModule,
    RouterOutlet,
    RouterLink,
    RouterLinkActive,
    CustomDatePickerComponent,
  ],
  templateUrl: './book-appointment.component.html',
  styleUrl: './book-appointment.component.css',
})
export class BookAppointmentComponent implements OnInit {
  errorWhileAddingAppointment: boolean = false;
  isLoading: boolean = false;

  @Input() targetServiceId!: string;

  isFetchingServices: boolean = false;
  isFetchingEmployee: boolean = false;
  isFetchingAvailableDates: boolean = false;
  isFetchingAvailableHours: boolean = false;
  isSpecialOffer: boolean = false;
  isPreferedEmployee: boolean = false;
  isPreferedService: boolean = false;
  isPayment: boolean = false;

  listServices: Service[] = [];
  listEmployees: Employee[] = [];
  validationErrors: ValidationErrors = {
    service: null,
    employee: null,
    date: null,
    startHour: null,
    endHour: null,
  };

  listAvailableSliceHour: {
    start: string;
    end: string;
  }[] = [];
  appointmentForm: FormGroup = new FormGroup({
    service: new FormControl(null, [Validators.required]),
    employee: new FormControl('', [Validators.required]),
    startHour: new FormControl('', [Validators.required]),
    endHour: new FormControl('', [Validators.required]),
  });
  errorInSliceHour: boolean = false;

  selectedDate: string = '';
  constructor(
    private sessionService: SessionService,
    private serviceService: ServiceService,
    private appointmentService: AddAppointmentService,
    private employeeService: EmployeeService,
    private router: Router,
    @Inject(PLATFORM_ID) private platformId: Object
  ) {}

  updateIsSpecialOffer() {
    if (this.checkSpecialOffer(this.appointmentForm.get('service')?.value)) {
      this.isSpecialOffer = true;
    } else {
      this.isSpecialOffer = false;
    }
  }
  updateListAvailableSliceHour(
    newAvailableSliceHour: {
      start: string;
      end: string;
    }[]
  ) {
    this.listAvailableSliceHour = newAvailableSliceHour;
  }

  updateSelectedDate(newDate: string) {
    this.selectedDate = newDate;
  }
  checkSpecialOffer(serviceId: string) {
    for (let i = 0; i < this.listServices.length; i++) {
      if (this.listServices[i]._id == serviceId) {
        if (this.listServices[i].type == 'special offer') {
          return true;
        }
        return false;
      }
    }
    return false;
  }
  checkIfPreference(id: string, categoryPreference: string) {
    if (categoryPreference == 'service') {
      for (let i = 0; i < this.listServices.length; i++) {
        if (this.listServices[i]._id == id) {
          if (this.listServices[i].servicePreference.length > 0) {
            this.isPreferedService = true;
            return true;
          }
          this.isPreferedService = false;
          return false;
        }
      }
    } else {
      for (let i = 0; i < this.listEmployees.length; i++) {
        if (this.listEmployees[i]._id == id) {
          if (this.listEmployees[i].employeePreference.length > 0) {
            this.isPreferedEmployee = true;
            return true;
          }
          this.isPreferedEmployee = false;
          return false;
        }
      }
    }
    return false;
  }
  ngOnInit(): void {
    if (isPlatformBrowser(this.platformId)) {
      const userId = this.sessionService.getData('id');
      this.serviceService.getAllServices(userId ?? '').subscribe({
        next: (v) => {
          this.isFetchingServices = false;
          if (v.body !== null && v.body.length > 0) {
            this.listServices = v.body;
            let serviceValue =
              this.targetServiceId == undefined || this.targetServiceId == ''
                ? this.listServices[0]?._id
                : this.targetServiceId;
            this.appointmentForm.get('service')?.setValue(serviceValue);
            if (this.checkSpecialOffer(serviceValue)) {
              this.isSpecialOffer = true;
            }

            this.checkIfPreference(
              this.appointmentForm.get('service')?.value,
              'service'
            );
          }
        },
        error: (e) => {
          console.log(e);
        },
      });

      this.employeeService.getAllEmployee(userId ?? '').subscribe({
        next: (v) => {
          this.isFetchingServices = false;
          if (v.body !== null && v.body.length > 0) {
            this.listEmployees = v.body;
            this.appointmentForm
              .get('employee')
              ?.setValue(this.listEmployees[0]?._id);
            this.checkIfPreference(
              this.appointmentForm.get('employee')?.value,
              'employee'
            );
          }
        },
        error: (e) => {
          console.log('error');
        },
      });
    }
  }
  validate() {
    let noErrors = true;
    if (this.appointmentForm.get('service')?.errors?.['required']) {
      this.validationErrors.service = 'Service is required';
      noErrors = false;
    }
    if (this.selectedDate == '') {
      this.validationErrors.date = 'Date is required';
      noErrors = false;
    }
    if (this.appointmentForm.get('startHour')?.errors?.['required']) {
      this.validationErrors.startHour = 'Start hour is required';
      noErrors = false;
    }
    if (this.appointmentForm.get('End hour')?.errors?.['required']) {
      this.validationErrors.endHour = 'End hour is required';
      noErrors = false;
    }
    if (!this.isDateInsideAvailableSliceHour()) {
      this.errorInSliceHour = true;
      noErrors = false;
    }
    return noErrors;
  }

  addAppointmentInList() {
    this.isLoading = true;
    let appointment = {
      user: this.sessionService.getData('id'),
      service: this.appointmentForm.value.service,
      status: 'create',
      employee: this.appointmentForm.value.employee,
    } as Appointment;

    let dates = [
      {
        start:
          this.selectedDate +
          'T' +
          this.appointmentForm.value.startHour +
          ':00.000',
        end:
          this.selectedDate +
          'T' +
          this.appointmentForm.value.endHour +
          ':00.000',
      },
    ];
    let data = {
      appointment: appointment,
      dates: dates,
    } as AddAppointment;

    this.appointmentService.addAppointment(data).subscribe({
      next: (v) => {
        this.isLoading = false;
        this.router.navigate(['home/userAppointment']);
      },
      error: (e) => {
        this.isLoading = false;
        this.errorWhileAddingAppointment = true;
        console.log('error');
      },
    });
  }
  goToPayment() {
    if (this.validate()) {
      this.isPayment = true;
    }
  }
  getCurrentServiceDuration() {
    for (let i = 0; i < this.listServices.length; i++) {
      if (this.listServices[i]._id == this.appointmentForm.value.service) {
        return this.listServices[i].duration;
      }
    }
    return null;
  }

  getCurrentService() {
    for (let i = 0; i < this.listServices.length; i++) {
      if (this.listServices[i]._id == this.appointmentForm.value.service) {
        return this.listServices[i];
      }
    }
    return null;
  }

  getCurrentEmployee() {
    for (let i = 0; i < this.listEmployees.length; i++) {
      if (this.listEmployees[i]._id == this.appointmentForm.value.employee) {
        return this.listEmployees[i];
      }
    }
    return null;
  }
  isDateInsideAvailableSliceHour() {
    let startHourString = this.appointmentForm.value.startHour + ':00';
    let endHourString = this.appointmentForm.value.endHour + ':00';
    console.log(this.getMinutesDifference(endHourString, startHourString));
    for (let i = 0; i < this.listAvailableSliceHour.length; i++) {
      if (
        this.getMinutesDifference(endHourString, startHourString) ==
          this.getCurrentServiceDuration() &&
        this.getMinutesDifference(
          startHourString,
          this.listAvailableSliceHour[i].start
        ) >= 0 &&
        this.getMinutesDifference(
          this.listAvailableSliceHour[i].end,
          endHourString
        ) >= 0
      ) {
        return true;
      }
    }
    return false;
  }
  getMinutesDifference(start: string, end: string) {
    const [hours1, minutes1, seconds1] = end.split(':');
    const [hours2, minutes2, seconds2] = start.split(':');
    const totalMinutes1 =
      parseInt(hours1) * 60 + parseInt(minutes1) + parseInt(seconds1) / 60;
    const totalMinutes2 =
      parseInt(hours2) * 60 + parseInt(minutes2) + parseInt(seconds2) / 60;

    const differenceInMinutes = Math.abs(totalMinutes1 - totalMinutes2);

    return differenceInMinutes;
  }
}
