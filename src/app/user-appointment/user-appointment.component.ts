import { NgForOf, NgIf } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { RouterLink, RouterLinkActive, RouterOutlet } from '@angular/router';
import SearchUserAppointmentResponse, {
  SearchUserAppointment,
} from '../services/appointment/user-appointment';
import { UserAppointmentService } from '../services/appointment/user-appointment.service';
import { SessionService } from '../services/authentication/session.service';
@Component({
  selector: 'app-user-appointment',
  standalone: true,
  imports: [
    NgForOf,
    NgIf,
    ReactiveFormsModule,
    RouterOutlet,
    RouterLink,
    RouterLinkActive,
  ],
  templateUrl: './user-appointment.component.html',
  styleUrl: './user-appointment.component.css',
})
export class UserAppointmentComponent implements OnInit {
  listAppointment: SearchUserAppointmentResponse[] = [];
  searchForm = new FormGroup({
    search: new FormControl(),
  });

  ngOnInit(): void {
    let today = new Date();
    today.setHours(today.getHours() + 3);
    let todayString =
      today.getFullYear() +
      '-' +
      (today.getMonth() + 1 < 10
        ? '0' + (today.getMonth() + 1)
        : today.getMonth() + 1 + '') +
      '-' +
      (today.getDate() < 10 ? '0' + today.getDate() : today.getDate() + '');
    this.searchForm
      .get('search')
      ?.setValue(
        todayString.split('-')[0] +
          '/' +
          todayString.split('-')[1] +
          '/' +
          todayString.split('-')[2]
      );
    const data = {
      userId: this.sesssionStore.getData('id'),
      date: todayString,
    } as SearchUserAppointment;
    this.userAppointmentService.searchUserAppointment(data).subscribe({
      next: (v) => {
        if (v.body !== null && v.body.length > 0) {
          this.listAppointment = v.body;
        }
      },
      error: (e) => {
        console.log('error');
      },
    });
  }
  constructor(
    private sesssionStore: SessionService,
    private userAppointmentService: UserAppointmentService
  ) {}

  search() {
    const data = {
      userId: this.sesssionStore.getData('id'),
      date: this.searchForm.get('search')?.value,
    } as SearchUserAppointment;
    this.userAppointmentService.searchUserAppointment(data).subscribe({
      next: (v) => {
        if (v.body !== null && v.body.length > 0) {
          this.listAppointment = v.body;
        }
      },
      error: (e) => {
        console.log('ici');
        this.listAppointment = [];
      },
    });
  }
  formatDate(dateString: string) {
    const daysOfWeek = [
      'Sunday',
      'Monday',
      'Tuesday',
      'Wednesday',
      'Thursday',
      'Friday',
      'Saturday',
    ];
    const months = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December',
    ];

    const date = new Date(dateString);
    const dayOfWeek = daysOfWeek[date.getDay()];
    const month = months[date.getMonth()];
    const dayOfMonth = date.getDate();
    const year = date.getFullYear();
    const hours = String(date.getHours()).padStart(2, '0');
    const minutes = String(date.getMinutes()).padStart(2, '0');

    return `${dayOfWeek} ${dayOfMonth} ${month} ${year} at ${hours}:${minutes}`;
  }
}
