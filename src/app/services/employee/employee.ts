export default interface Employee {
  _id: string;
  name: string;
  email: string;
  password: string;
  role: string;
  etat?: string;
  employeePreference: EmployeePreference[] | [];
}

export interface EmployeePreference {
  _id: string;
  userId: string;
  employeeId: string;
}
