import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, throwError } from 'rxjs';
import { environment } from '../../../environments/environment';
import Employee from './employee';
@Injectable({
  providedIn: 'root',
})
export class EmployeeService {
  httpHeaders: HttpHeaders = new HttpHeaders({
    'Access-Control-Allow-Origin': '*',
  });
  constructor(private http: HttpClient) {}

  getAllEmployee(userId: string): Observable<HttpResponse<Employee[]>> {
    return this.http
      .get<Employee[]>(environment.apiUrl + '/employee/' + userId, {
        headers: this.httpHeaders,
        observe: 'response',
      })
      .pipe(
        catchError((e) => {
          console.log(e);
          return throwError(
            () => new Error('ERROR WHILE GETTING ALL EMPLOYEES')
          );
        })
      );
  }
}
