import {  Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SessionService } from './session.service';
import { Provider } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { isPlatformBrowser } from '@angular/common';
import { platform } from 'os';
@Injectable({
  providedIn: 'root'
})
export class AuthenticationInterceptor implements HttpInterceptor{
  isBrowser:boolean
  constructor(private sessionStore: SessionService,@Inject(PLATFORM_ID) platformId:Object){ 
    this.isBrowser=isPlatformBrowser(platformId)
  }
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if(this.isBrowser){
      const token = this.sessionStore.getData("token");
      if(token!==undefined){
        const authReq = request.clone({
          setHeaders: {
            Authorization: `Bearer ${token}`
          }
        });
        return next.handle(authReq);
      }
    }
      return next.handle(request);
  }
}

export const AuthenticationInterceptorProvider: Provider =
  { provide: HTTP_INTERCEPTORS, useClass: AuthenticationInterceptor, multi: true };