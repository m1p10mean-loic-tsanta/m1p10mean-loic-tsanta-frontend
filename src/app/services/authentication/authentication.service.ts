import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import User, { UserLogin, UserLoginResponse, UserRegisterResponse} from './authentification';
import { catchError,Observable, throwError } from 'rxjs';
import { environment } from '../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  httpHeaders: HttpHeaders = new HttpHeaders({
    "Access-Control-Allow-Origin": '*'
  });
  constructor(private http: HttpClient) { }


  login(user : UserLogin):Observable<HttpResponse<UserLoginResponse>> {
    return this.http.post<UserLoginResponse >(environment.apiUrl+"/user/login",user,{headers:this.httpHeaders,observe  :'response'}).pipe(
      catchError(()=>{
        return throwError(()=>new Error("ERROR WHILE LOG IN"))
      })
    );
  }

  register(user : Partial<User>):Observable<HttpResponse<UserRegisterResponse>> {
    return this.http.post<UserRegisterResponse >(environment.apiUrl+"/user/register",user,{headers:this.httpHeaders,observe  :'response'}).pipe(
      catchError(()=>{
        return throwError(()=>new Error("ERROR WHILE REGISTER"))
      })
    );
  }
}
