export default interface User {
    _id: string,
    name: string,
    email: string,
    password: string,
    role: string
}

export  interface UserData {
    data:User[]
}

export interface UserLogin {
    email: string,
    password: string
}

export interface UserLoginResponse {
    user: User,
    token: string
}

export interface UserRegisterResponse {
    id: string,
    role: string,
    token: string,
}