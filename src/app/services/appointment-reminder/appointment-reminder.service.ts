import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders ,HttpResponse} from '@angular/common/http';
import AppointmentReminderResponse from './appointment-reminder';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AppointmentReminderService {

  
  httpHeaders: HttpHeaders = new HttpHeaders({
    "Access-Control-Allow-Origin": '*'
  });
  constructor(private http: HttpClient) { }


  getCloseAppointments(userId:string):Observable<HttpResponse<AppointmentReminderResponse[]>> {
    return this.http.get<AppointmentReminderResponse[] >(environment.apiUrl+"/remind-appointment/"+userId,{headers:this.httpHeaders,observe  :'response'})
  }
}
