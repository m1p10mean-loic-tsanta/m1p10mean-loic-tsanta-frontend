import { TestBed } from '@angular/core/testing';

import { AppointmentReminderService } from './appointment-reminder.service';

describe('AppointmentReminderService', () => {
  let service: AppointmentReminderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AppointmentReminderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
