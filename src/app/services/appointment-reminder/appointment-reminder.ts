import Service from "../service/service";

export default interface AppointmentReminderResponse{
    _id: string;
    appointment: string;
    start: string;
    end: string;
    startDate: string;
    endDate: string;
    appointmentDetails:Appointment[]
    beginningInfToday: boolean;
    dateDistance: number;
    serviceDetails: Service[];
}

export interface Appointment{
    _id: string
    user:string
    service:string
    status:string
}