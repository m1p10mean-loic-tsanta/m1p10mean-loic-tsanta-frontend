import { TestBed } from '@angular/core/testing';

import { EmployeeAppointmentService } from './employee-appointment.service';

describe('EmployeeAppointmentService', () => {
  let service: EmployeeAppointmentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EmployeeAppointmentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
