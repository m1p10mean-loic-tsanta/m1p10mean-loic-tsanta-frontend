export default interface UnvavailableDateData {
  month: number,
  year: number,
  employeeId: string,
  serviceId: string,
}

export interface AvailableSliceHourData {
 date:string
  employeeId: string,
  serviceId: string,
}


