export default interface SearchEmployeeAppointmentResponse {
  _id: string;
  service: string;
  start: string;
  end: string;
  client: string;
}

export interface SearchEmployeeAppointment {
  employeeId: string;
  date: string;
}
