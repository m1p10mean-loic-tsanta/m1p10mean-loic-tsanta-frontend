import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, throwError } from 'rxjs';
import { environment } from '../../../environments/environment';
import { AddAppointment, AddAppointmentResponse } from './add-appointment';

@Injectable({
  providedIn: 'root'
})
export class AddAppointmentService {
  httpHeaders: HttpHeaders = new HttpHeaders({
    "Access-Control-Allow-Origin": '*'
  });
  constructor(private http: HttpClient) { }

  addAppointment(data : AddAppointment):Observable<HttpResponse<AddAppointmentResponse>> {
    return this.http.post<AddAppointmentResponse >(environment.apiUrl+"/appointment",data,{headers:this.httpHeaders,observe  :'response'}).pipe(
      catchError(()=>{
        return throwError(()=>new Error("ERROR WHILE ADDING NEW APPOINTMENT"))
      })
    );
  }
}
