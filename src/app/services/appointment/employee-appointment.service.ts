import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, throwError } from 'rxjs';
import { environment } from '../../../environments/environment';
import SearchEmployeeAppointmentResponse, {
  SearchEmployeeAppointment,
} from './employee-appointment';

@Injectable({
  providedIn: 'root',
})
export class EmployeeAppointmentService {
  httpHeaders: HttpHeaders = new HttpHeaders({
    'Access-Control-Allow-Origin': '*',
  });
  constructor(private http: HttpClient) {}

  searchEmployeeAppointment(
    parameter: SearchEmployeeAppointment
  ): Observable<HttpResponse<SearchEmployeeAppointmentResponse[]>> {
    return this.http
      .get<SearchEmployeeAppointmentResponse[]>(
        environment.apiUrl +
          '/employee/search-appointment/' +
          parameter.employeeId +
          '/' +
          parameter.date,
        { headers: this.httpHeaders, observe: 'response' }
      )
      .pipe(
        catchError(() => {
          return throwError(
            () => new Error('ERROR WHILE GETTING EMPLOYEE APPOINTMENT')
          );
        })
      );
  }
}
