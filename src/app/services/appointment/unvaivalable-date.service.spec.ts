import { TestBed } from '@angular/core/testing';

import { UnvaivalableDateService } from './unvaivalable-date.service';

describe('UnvaivalableDateService', () => {
  let service: UnvaivalableDateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UnvaivalableDateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
