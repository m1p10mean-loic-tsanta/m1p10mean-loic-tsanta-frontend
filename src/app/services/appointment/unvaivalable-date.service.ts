import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { catchError,Observable, throwError } from 'rxjs';
import { environment } from '../../../environments/environment';
import UnvavailableDateData from './unvaivalable-date';
import {AvailableSliceHourData} from './unvaivalable-date';

@Injectable({
  providedIn: 'root'
})
export class UnvaivalableDateService {
  httpHeaders: HttpHeaders = new HttpHeaders({
    "Access-Control-Allow-Origin": '*'
  });
  constructor(private http: HttpClient) { }

  getUnavailableDatesOfMonth(data : UnvavailableDateData):Observable<HttpResponse<string[]>> {
    return this.http.post<string[] >(environment.apiUrl+"/unavailableDateOfMonth",data,{headers:this.httpHeaders,observe  :'response'}).pipe(
      catchError(()=>{
        return throwError(()=>new Error("ERROR WHILE FETCHING UNAVAILABLE DATES OF MONTH"))
      })
    );
  }
  getAvailableSliceHour(data : AvailableSliceHourData):Observable<HttpResponse<{
    start:string,
    end:string
  }[]>> {
    return this.http.post<{
      start:string,
      end:string
    }[] >(environment.apiUrl+"/available-slice-hour",data,{headers:this.httpHeaders,observe  :'response'}).pipe(
      catchError(()=>{
        return throwError(()=>new Error("ERROR WHILE FETCHING AVAILABLE SLICE HOUR OF MONTH"))
      })
    );
  }

}
