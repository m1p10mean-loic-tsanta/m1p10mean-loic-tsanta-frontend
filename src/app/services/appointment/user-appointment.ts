export default interface SearchUserAppointmentResponse {
  _id: string;
  service: string;
  start: string;
  end: string;
  employee: string;
}

export interface SearchUserAppointment {
  userId: string;
  date: string;
}
