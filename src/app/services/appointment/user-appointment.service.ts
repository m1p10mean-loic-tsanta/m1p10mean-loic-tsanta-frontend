import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, throwError } from 'rxjs';
import { environment } from '../../../environments/environment';
import SearchUserAppointmentResponse, {
  SearchUserAppointment,
} from './user-appointment';

@Injectable({
  providedIn: 'root',
})
export class UserAppointmentService {
  httpHeaders: HttpHeaders = new HttpHeaders({
    'Access-Control-Allow-Origin': '*',
  });
  constructor(private http: HttpClient) {}

  searchUserAppointment(
    parameter: SearchUserAppointment
  ): Observable<HttpResponse<SearchUserAppointmentResponse[]>> {
    return this.http
      .get<SearchUserAppointmentResponse[]>(
        environment.apiUrl +
          '/user/search-appointment/' +
          parameter.userId +
          '/' +
          parameter.date,
        { headers: this.httpHeaders, observe: 'response' }
      )
      .pipe(
        catchError(() => {
          return throwError(
            () => new Error('ERROR WHILE GETTING USER APPOINTMENT')
          );
        })
      );
  }
}
