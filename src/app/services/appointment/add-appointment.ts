export default interface Appointment{
    user: string;
    service: string;
    status: string;
    employee: string;
}
export interface AddAppointment{
    appointment:Appointment;
    dates:AppointmentDates[]
}
export interface AppointmentDates{
    start:string;
    end:string
}

export interface AddAppointmentResponse {
    message: string;
}