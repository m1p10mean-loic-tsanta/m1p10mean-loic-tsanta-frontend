import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import UserEmployeePreference, {
  AddUserPreferenceResponse,
  DeleteUserPreferenceResponse,
  UserServicePreference,
} from './user-preferences';

@Injectable({
  providedIn: 'root',
})
export class UserPreferencesService {
  httpHeaders: HttpHeaders = new HttpHeaders({
    'Access-Control-Allow-Origin': '*',
  });
  constructor(private http: HttpClient) {}

  addUserServicePreference(
    data: UserServicePreference
  ): Observable<HttpResponse<AddUserPreferenceResponse[]>> {
    return this.http.post<AddUserPreferenceResponse[]>(
      environment.apiUrl + '/user/add-service-preference',
      data,
      { headers: this.httpHeaders, observe: 'response' }
    );
  }
  addUserEmployeePreference(
    data: UserEmployeePreference
  ): Observable<HttpResponse<AddUserPreferenceResponse[]>> {
    return this.http.post<AddUserPreferenceResponse[]>(
      environment.apiUrl + '/user/add-employee-preference',
      data,
      { headers: this.httpHeaders, observe: 'response' }
    );
  }

  getUserServicePreference(
    userId: string
  ): Observable<HttpResponse<UserServicePreference[]>> {
    return this.http.get<UserServicePreference[]>(
      environment.apiUrl + '/user/service-preference/' + userId,
      { headers: this.httpHeaders, observe: 'response' }
    );
  }
  getUserEmployeePreference(
    userId: string
  ): Observable<HttpResponse<UserEmployeePreference[]>> {
    return this.http.get<UserEmployeePreference[]>(
      environment.apiUrl + '/user/employee-preference/' + userId,
      { headers: this.httpHeaders, observe: 'response' }
    );
  }

  deleteUserServicePreference(
    id: string,
    userId: string
  ): Observable<HttpResponse<DeleteUserPreferenceResponse[]>> {
    return this.http.delete<DeleteUserPreferenceResponse[]>(
      environment.apiUrl + '/user/service-preference/' + id + '/' + userId,
      { headers: this.httpHeaders, observe: 'response' }
    );
  }
  deleteUserEmployeePreference(
    id: string,
    userId: string
  ): Observable<HttpResponse<DeleteUserPreferenceResponse[]>> {
    return this.http.delete<DeleteUserPreferenceResponse[]>(
      environment.apiUrl + '/user/employee-preference/' + id + '/' + userId,
      { headers: this.httpHeaders, observe: 'response' }
    );
  }
}
