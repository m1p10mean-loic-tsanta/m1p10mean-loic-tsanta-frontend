export default interface UserEmployeePreference {
  userId: string;
  employeeId: string;
}

export interface UserServicePreference {
  userId: string;
  serviceId: string;
}

export interface AddUserPreferenceResponse {
  id: string;
}
export interface DeleteUserPreferenceResponse {
  message: string;
}
