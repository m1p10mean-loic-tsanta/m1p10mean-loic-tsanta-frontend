export default interface SpecialOfferResponse{
    _id: string;
    name: string;
    duration: number;
    price: number;
    commission: number;
    type: string;
    beginning: string;
    expiration: string;
    beginningDate: string;
    expirationDate: string;
    supToBeginnig: boolean;
    infToExpiration: boolean
}