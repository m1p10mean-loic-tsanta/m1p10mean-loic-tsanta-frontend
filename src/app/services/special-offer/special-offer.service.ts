import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders ,HttpResponse} from '@angular/common/http';
import SpecialOfferResponse from './special-offer';
import { Observable} from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SpecialOfferService {

  httpHeaders: HttpHeaders = new HttpHeaders({
    "Access-Control-Allow-Origin": '*'
  });
  constructor(private http: HttpClient) { }


  getCurrentSpecialOffers():Observable<HttpResponse<SpecialOfferResponse[]>> {
    return this.http.get<SpecialOfferResponse[] >(environment.apiUrl+"/currentSpecialOffer",{headers:this.httpHeaders,observe  :'response'})
  }
}
