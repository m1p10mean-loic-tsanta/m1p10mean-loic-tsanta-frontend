import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, throwError } from 'rxjs';
import { environment } from '../../../environments/environment';
import Service from './service';
@Injectable({
  providedIn: 'root',
})
export class ServiceService {
  httpHeaders: HttpHeaders = new HttpHeaders({
    'Access-Control-Allow-Origin': '*',
  });
  constructor(private http: HttpClient) {}

  getAllServices(userId: string): Observable<HttpResponse<Service[]>> {
    return this.http
      .get<Service[]>(environment.apiUrl + '/available-service/' + userId, {
        headers: this.httpHeaders,
        observe: 'response',
      })
      .pipe(
        catchError((e) => {
          console.log(e);
          return throwError(
            () => new Error('ERROR WHILE GETTING ALL SERVICES')
          );
        })
      );
  }

  getDefaultServices(userId: string): Observable<HttpResponse<Service[]>> {
    return this.http
      .get<Service[]>(environment.apiUrl + '/default-service/' + userId, {
        headers: this.httpHeaders,
        observe: 'response',
      })
      .pipe(
        catchError((e) => {
          console.log(e);
          return throwError(
            () => new Error('ERROR WHILE GETTING ALL DEFAULT SERVICES')
          );
        })
      );
  }

  searchServices(searchString: string): Observable<HttpResponse<Service[]>> {
    return this.http
      .get<Service[]>(environment.apiUrl + '/search-service/' + searchString, {
        headers: this.httpHeaders,
        observe: 'response',
      })
      .pipe(
        catchError((e) => {
          console.log(e);
          return throwError(() => new Error('ERROR WHILE SEARCHING SERVICES'));
        })
      );
  }
}
