export default interface Service {
  _id: string;
  name: string;
  duration: number;
  price: number;
  commission: number;
  type: string;
  servicePreference: ServicePreference[] | [];
}

export interface ServicePreference {
  _id: string;
  userId: string;
  serviceId: string;
}
