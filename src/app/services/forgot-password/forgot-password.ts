export default interface sendEmailToResetPasswordResponse {
  message:string
}
export interface resetPasswordResponse {
  message:string
}
export interface resetPasswordData {
  password:string
}