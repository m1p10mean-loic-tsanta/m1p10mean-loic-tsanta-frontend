

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { catchError,Observable, throwError } from 'rxjs';
import { environment } from '../../../environments/environment';
import sendEmailToResetPasswordResponse from './forgot-password';
import { resetPasswordResponse, resetPasswordData } from './forgot-password';

@Injectable({
  providedIn: 'root'
})
export class ForgotPasswordService {

  httpHeaders: HttpHeaders = new HttpHeaders({
    "Access-Control-Allow-Origin": '*'
  });
  constructor(private http: HttpClient) { }

  sendEmailToResetPassword(email : string):Observable<HttpResponse<sendEmailToResetPasswordResponse>> {
    return this.http.get<sendEmailToResetPasswordResponse >(environment.apiUrl+"/send-email-reset-password/"+email,{headers:this.httpHeaders,observe  :'response'}).pipe(
      catchError(()=>{
        return throwError(()=>new Error("EMAIL NOT SENDED OR EMAIL NOT FOUND"))
      })
    );
  }

  resetPassword(userId:string,password : resetPasswordData):Observable<HttpResponse<resetPasswordResponse>> {
    return this.http.patch<resetPasswordResponse >(environment.apiUrl+"/reset-password/"+userId,password,{headers:this.httpHeaders,observe  :'response'}).pipe(
      catchError(()=>{
        return throwError(()=>new Error("ERROR WHILE RESETING PASSWORD"))
      })
    );
  }

}
