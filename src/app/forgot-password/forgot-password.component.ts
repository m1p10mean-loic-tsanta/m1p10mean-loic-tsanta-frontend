import { Component } from '@angular/core';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { Router, RouterLink, RouterLinkActive, RouterOutlet } from '@angular/router';
import { NgForOf, NgIf } from '@angular/common';
import { ForgotPasswordService } from '../services/forgot-password/forgot-password.service';

@Component({
  selector: 'app-forgot-password',
  standalone: true,
  imports: [NgForOf,NgIf,ReactiveFormsModule, RouterOutlet, RouterLink, RouterLinkActive],
  templateUrl: './forgot-password.component.html',
  styleUrl: './forgot-password.component.css'
})
export class ForgotPasswordComponent {
  emailSendingLoading:boolean=false
  errorWhileSendingEmail:string=""
  successMessage:boolean=false
  emailForm = new FormGroup({
    email: new FormControl('',Validators.email),
  });
  constructor(private service: ForgotPasswordService,private router: Router){

  }

  sendEmail(){
    this.emailSendingLoading=true
    if(this.emailForm.value.email!=undefined&&this.emailForm.value.email!=""){
      this.service.sendEmailToResetPassword(this.emailForm.value.email).subscribe({
        next: (v) =>{
          this.errorWhileSendingEmail=""
          this.emailSendingLoading=false
          this.successMessage=true
        },
        error: (e) =>{ 
          this.errorWhileSendingEmail="Error while sending email"
          this.emailSendingLoading=false
        },
      })
    }
    else{
      this.errorWhileSendingEmail="Error while sending email"
      this.emailSendingLoading=false

    }
  }
}
