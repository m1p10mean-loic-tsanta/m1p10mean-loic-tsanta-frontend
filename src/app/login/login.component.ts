import { NgForOf, NgIf } from '@angular/common';
import { Component } from '@angular/core';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import {
  Router,
  RouterLink,
  RouterLinkActive,
  RouterOutlet,
} from '@angular/router';
import { AuthenticationService } from '../services/authentication/authentication.service';
import { UserLogin } from '../services/authentication/authentification';
import { SessionService } from '../services/authentication/session.service';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [
    NgForOf,
    NgIf,
    ReactiveFormsModule,
    RouterOutlet,
    RouterLink,
    RouterLinkActive,
  ],
  templateUrl: './login.component.html',
  styleUrl: './login.component.css',
})
export class LoginComponent {
  errorWhileLogging: boolean = false;
  isLogging: boolean = false;
  loginForm = new FormGroup({
    email: new FormControl(''),
    password: new FormControl(''),
  });

  constructor(
    private service: AuthenticationService,
    private router: Router,
    private sessionStore: SessionService
  ) {}

  login() {
    this.isLogging = true;
    const user = {
      email: this.loginForm.value.email,
      password: this.loginForm.value.password,
    } as UserLogin;
    this.service.login(user).subscribe({
      next: (v) => {
        this.errorWhileLogging = false;
        this.isLogging = false;
        if (v.body !== null) {
          this.sessionStore.saveData('token', v.body.token);
          this.sessionStore.saveData('id', v.body.user._id);
          this.sessionStore.saveData('role', v.body.user.role);
        }
        if (this.sessionStore.getData('role') == 'user') {
          this.router.navigate(['home/serviceList']);
        } else if (this.sessionStore.getData('role') == 'admin') {
          this.router.navigate(['admin']);
        } else {
          this.router.navigate(['home/employeeAppointment']);
        }
      },
      error: (e) => {
        this.errorWhileLogging = true;
        this.isLogging = false;
      },
    });
  }
}
