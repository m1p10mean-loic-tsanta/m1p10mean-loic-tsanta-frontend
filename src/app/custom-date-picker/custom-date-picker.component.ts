import { NgForOf, NgIf } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { UnvaivalableDateService } from '../services/appointment/unvaivalable-date.service';
import UnvavailableDateData, { AvailableSliceHourData } from '../services/appointment/unvaivalable-date';
import { throws } from 'assert';


@Component({
  selector: 'app-custom-date-picker',
  standalone: true,
  imports: [NgForOf,NgIf],
  templateUrl: './custom-date-picker.component.html',
  styleUrl: './custom-date-picker.component.css'
})
export class CustomDatePickerComponent implements OnInit {
  @Input() service:string='';
  @Input() employee:string='';
  month:number;
  monthName:string;
  year:number ;
  todayMonth:number;
  todayYear:number;
  todayDay:number;
  dayNameOfTheWeek = ["MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"];
  dayNameOfTheFirstOfTheMonth:string;
  numberOfDayOfTheMonth:number;
  arrayOfDispositionOfDay:(string | number)[]
  isLoading:boolean=false
  unavailableDates:string[]=[]
  selectedDate:string=''

  @Output() refreshAvailableHourSlice = new EventEmitter<{
    start:string,
    end:string
  }[]>();
  @Output() refreshSelectedDate = new EventEmitter<string>();

  getHourSliceAvailable() {
    const data={
      date:this.formatSelectedDate(),
      employeeId: this.employee+"",
      serviceId: this.service+"",
    } as AvailableSliceHourData
    this.unvaivalableDateService.getAvailableSliceHour(data).subscribe({
      next: (v) =>{
          if(v.body!==null&&v.body.length>0){
            this.refreshAvailableHourSlice.emit(v.body);
            this.refreshSelectedDate.emit(this.formatSelectedDate());
          }
        },
        error: (e) =>{ 
          console.log(e)
        },
      })
  }
  formatSelectedDate(){
    let month=(this.month<10?"0":"")+this.month
    let selectedDate=(this.selectedDate.length<2?"0":"")+this.selectedDate
    return this.year+"-"+month+"-"+selectedDate
  }

  constructor(private unvaivalableDateService: UnvaivalableDateService) {
    const date=new Date()
    date.setHours(date.getHours()+3)
    this.month=date.getMonth() + 1
    this.monthName=this.getMonthName(date.getMonth() + 1)
    this.year=date.getFullYear()
    this.todayMonth=this.month
    this.todayYear=this.year
    this.todayDay=date.getDate()
    this.numberOfDayOfTheMonth=new Date(this.year, this.month, 0).getDate()
    date.setDate(1)
    this.dayNameOfTheFirstOfTheMonth=this.getDayName(date)

    this.arrayOfDispositionOfDay=this.getArrayOfDispositionOfDay(this.numberOfDayOfTheMonth,this.dayNameOfTheFirstOfTheMonth)
   }
  ngOnInit(): void {

  }
  ngOnChanges(changes: SimpleChanges) {
    if(this.employee!=""&&this.service!=""){
      this.getUnavailableDatesOfMonth()
    }
  }
  infToToday(day:string|number){
    day=parseInt(day+"")
    if(day<this.todayDay&&this.month==this.todayMonth&&this.year==this.todayYear){
      return true
    }
    return false
  }
  getUnavailableDatesOfMonth(){
    const data={
      month: this.month,
      year: this.year,
      employeeId: this.employee+"",
      serviceId: this.service+"",
    } as UnvavailableDateData
    this.unvaivalableDateService.getUnavailableDatesOfMonth(data).subscribe({
      next: (v) =>{
        if(v.body!==null&&v.body.length>0){
            this.unavailableDates=v.body
        }
        else{
          this.unavailableDates=[]
        }
          this.updateAllDataAfterMonthYearChange()
          this.refreshAvailableHourSlice.emit([]);
          
        },
        error: (e) =>{ 
          console.log(e)
        },
      })
    }

  getMonthName(monthInNumber: number) {
    switch (monthInNumber) {
        case 1:
            return "January";
        case 2:
            return "February";
        case 3:
            return "March";
        case 4:
            return "April";
        case 5:
            return "May";
        case 6:
            return "June";
        case 7:
            return "July";
        case 8:
            return "August";
        case 9:
            return "September";
        case 10:
            return "October";
        case 11:
            return "November";
        case 12:
            return "December";
    }
    return ""
  }
  getDayName(date :Date, locale = "en-US") {
    return date.toLocaleDateString(locale, { weekday: "long" });
  }

  getArrayOfDispositionOfDay(
    numberOfDayOfTheMonth: number,
    dayNameOfTheFirstOfTheMonth: string
) {
    const dayNameOfTheWeek = [
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday",
        "Sunday",
    ];
    const finalTab = [];
    for (let index = 0; index < numberOfDayOfTheMonth; index++) {
        finalTab.push(index + 1);
    }
    for (
        let index = 0;
        index < dayNameOfTheWeek.indexOf(dayNameOfTheFirstOfTheMonth);
        index++
    ) {
        finalTab.unshift("");
    }
    const currentFinalTabLength = finalTab.length;
    for (let index = 0; index < 42 - currentFinalTabLength; index++) {
        finalTab.push("");
    }
    return finalTab;
  }
  switchMonth(increase: boolean) {
    if (increase) {
      if (this.month === 12) {
        this.month=1;
        this.year+= 1;
        this.getUnavailableDatesOfMonth()
          return;
      }
        this.month+= 1;
    } else {
        if (this.month === 1) {
          this.month=12;
          this.year-= 1;
          this.getUnavailableDatesOfMonth()
            return;
        }
        this.month-= 1;
    }
    this.getUnavailableDatesOfMonth()

  }
  switchYear(increase: boolean) {
    if (increase) {
      this.year+= 1;
      
    } else {
      if (this.year !== 1) {
        this.year-= 1;
      }
    }
    this.getUnavailableDatesOfMonth()
  }
  updateAllDataAfterMonthYearChange(){
    this.setDateSelected('')
    const date=new Date()
    date.setHours(date.getHours()+3)
    date.setMonth(this.month-1)
    date.setFullYear(this.year)
    this.monthName=this.getMonthName(this.month)

    this.numberOfDayOfTheMonth=new Date(this.year, this.month, 0).getDate()
    date.setDate(1)
    this.dayNameOfTheFirstOfTheMonth=this.getDayName(date)
    this.arrayOfDispositionOfDay=this.getArrayOfDispositionOfDay(this.numberOfDayOfTheMonth,this.dayNameOfTheFirstOfTheMonth)
  }
  isDateInsideUnavailableDate(day:number|string){
    for (let i = 0; i < this.unavailableDates.length; i++) {
      if(parseInt(this.unavailableDates[i].split("-")[2]).toString()==day+""){
        return true
      }
    }
    return false
  }
  isDateSelected(day:number|string){
    if(this.selectedDate==day+''){
      return true
    }
    return false
  }
  setDateSelected(day:number|string){
    this.selectedDate=day+''
    if(this.selectedDate!=''){
      this.getHourSliceAvailable()
    }
  }
}
