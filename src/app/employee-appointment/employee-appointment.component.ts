import { NgForOf, NgIf } from '@angular/common';
import { Component } from '@angular/core';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import {
  Router,
  RouterLink,
  RouterLinkActive,
  RouterOutlet,
} from '@angular/router';
import SearchEmployeeAppointmentResponse, {
  SearchEmployeeAppointment,
} from '../services/appointment/employee-appointment';
import { EmployeeAppointmentService } from '../services/appointment/employee-appointment.service';
import { SessionService } from '../services/authentication/session.service';

@Component({
  selector: 'app-employee-appointment',
  standalone: true,
  imports: [
    NgForOf,
    NgIf,
    ReactiveFormsModule,
    RouterOutlet,
    RouterLink,
    RouterLinkActive,
  ],
  templateUrl: './employee-appointment.component.html',
  styleUrl: './employee-appointment.component.css',
})
export class EmployeeAppointmentComponent {
  constructor(
    private employeeAppointmentService: EmployeeAppointmentService,
    private router: Router,
    private sesssionStore: SessionService
  ) {}

  listAppointment: SearchEmployeeAppointmentResponse[] = [];
  searchForm = new FormGroup({
    search: new FormControl(),
  });
  dateIsToday: boolean = true;
  today: string = '';
  ngOnInit(): void {
    let today = new Date();
    today.setHours(today.getHours() + 3);
    let todayString =
      today.getFullYear() +
      '-' +
      (today.getMonth() + 1 < 10
        ? '0' + (today.getMonth() + 1)
        : today.getMonth() + 1 + '') +
      '-' +
      (today.getDate() < 10 ? '0' + today.getDate() : today.getDate() + '');
    this.searchForm
      .get('search')
      ?.setValue(
        todayString.split('-')[0] +
          '/' +
          todayString.split('-')[1] +
          '/' +
          todayString.split('-')[2]
      );
    this.today = todayString;
    const data = {
      employeeId: this.sesssionStore.getData('id'),
      date: todayString,
    } as SearchEmployeeAppointment;
    this.employeeAppointmentService.searchEmployeeAppointment(data).subscribe({
      next: (v) => {
        if (v.body !== null && v.body.length > 0) {
          this.listAppointment = v.body;
        }
      },
      error: (e) => {
        console.log('error');
      },
    });
  }
  isSearchDateToday() {
    if (this.searchForm.get('search')?.value == this.today) {
      this.dateIsToday = true;
    } else {
      this.dateIsToday = false;
    }
  }

  search() {
    const data = {
      employeeId: this.sesssionStore.getData('id'),
      date: this.searchForm.get('search')?.value,
    } as SearchEmployeeAppointment;
    this.employeeAppointmentService.searchEmployeeAppointment(data).subscribe({
      next: (v) => {
        if (v.body !== null && v.body.length > 0) {
          this.listAppointment = v.body;
        }
      },
      error: (e) => {
        console.log('ici');
        this.listAppointment = [];
      },
    });
  }
  formatDate(dateString: string) {
    const daysOfWeek = [
      'Sunday',
      'Monday',
      'Tuesday',
      'Wednesday',
      'Thursday',
      'Friday',
      'Saturday',
    ];
    const months = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December',
    ];

    const date = new Date(dateString);
    const dayOfWeek = daysOfWeek[date.getDay()];
    const month = months[date.getMonth()];
    const dayOfMonth = date.getDate();
    const year = date.getFullYear();
    const hours = String(date.getHours()).padStart(2, '0');
    const minutes = String(date.getMinutes()).padStart(2, '0');

    return `${dayOfWeek} ${dayOfMonth} ${month} ${year} at ${hours}:${minutes}`;
  }
  goToTaskDetails(appointmentId: string) {
    this.router.navigate(['home/employee-appointment-task', appointmentId]);
  }
}
