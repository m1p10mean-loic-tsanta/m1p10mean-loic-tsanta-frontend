import { isPlatformBrowser, NgForOf, NgIf } from '@angular/common';
import { Component, Inject, OnInit, PLATFORM_ID } from '@angular/core';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { RouterLink, RouterLinkActive, RouterOutlet } from '@angular/router';
import { SessionService } from '../services/authentication/session.service';
import Service from '../services/service/service';
import { ServiceService } from '../services/service/service.service';

@Component({
  selector: 'app-service',
  standalone: true,
  imports: [
    NgForOf,
    NgIf,
    ReactiveFormsModule,
    RouterOutlet,
    RouterLink,
    RouterLinkActive,
  ],
  templateUrl: './service.component.html',
  styleUrl: './service.component.css',
})
export class ServiceComponent implements OnInit {
  listServices: Service[] = [];

  searchForm = new FormGroup({
    search: new FormControl(''),
  });
  constructor(
    private serviceService: ServiceService,
    private sessionService: SessionService,
    @Inject(PLATFORM_ID) private platformId: Object
  ) {}
  ngOnInit(): void {
    if (isPlatformBrowser(this.platformId)) {
      const userId = this.sessionService.getData('id');
      this.serviceService.getAllServices(userId ?? '').subscribe({
        next: (v) => {
          if (v.body !== null && v.body.length > 0) {
            this.listServices = v.body;
          }
        },
        error: (e) => {
          console.log('error');
        },
      });
    }
  }
  search() {
    const searchString = this.searchForm.value.search ?? '';
    if (searchString == '') {
      return;
    }
    this.serviceService.searchServices(searchString).subscribe({
      next: (v) => {
        if (v.body !== null && v.body.length > 0) {
          this.listServices = v.body;
        } else {
          this.listServices = [];
        }
      },
      error: (e) => {
        console.log('error');
      },
    });
  }
}
