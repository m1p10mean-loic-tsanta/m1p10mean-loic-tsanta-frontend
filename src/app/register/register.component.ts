import { NgForOf, NgIf } from '@angular/common';
import { Component } from '@angular/core';
import { FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
import { Router, RouterLink, RouterLinkActive, RouterOutlet } from '@angular/router';
import { AuthenticationService } from '../services/authentication/authentication.service';
import User from '../services/authentication/authentification';
import { SessionService } from '../services/authentication/session.service';

@Component({
  selector: 'app-register',
  standalone: true,
  imports: [NgForOf,NgIf,ReactiveFormsModule,RouterOutlet, RouterLink, RouterLinkActive],
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent {
  isRegistering:boolean=false
  errorWhileRegister: boolean = false
  registerForm = new FormGroup({
    firstName: new FormControl(''),
    lastName: new FormControl(''),
    email: new FormControl(''),
    password: new FormControl(''),
  });

  constructor(private service: AuthenticationService,private router: Router,private sessionStore: SessionService) { }

  register(){
    this.isRegistering=true
    const user={
      name: this.registerForm.value.firstName+" "+this.registerForm.value.lastName,
      email: this.registerForm.value.email,
      password: this.registerForm.value.password,
      role: "user"
    } as Partial<User>
    this.service.register(user).subscribe({
      next: (v) =>{
        this.errorWhileRegister=false
        this.isRegistering=false
        if(v.body!==null){
          this.sessionStore.saveData("token",v.body.token)
          this.sessionStore.saveData("id",v.body.id)
          this.sessionStore.saveData("role",v.body.role)
        }
        this.router.navigate([''])
      },
      error: (e) => {
        this.errorWhileRegister=true
        this.isRegistering=false
      },
    })
  }
}
