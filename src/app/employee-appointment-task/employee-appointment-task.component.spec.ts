import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeAppointmentTaskComponent } from './employee-appointment-task.component';

describe('EmployeeAppointmentTaskComponent', () => {
  let component: EmployeeAppointmentTaskComponent;
  let fixture: ComponentFixture<EmployeeAppointmentTaskComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [EmployeeAppointmentTaskComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(EmployeeAppointmentTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
