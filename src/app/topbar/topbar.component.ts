import { isPlatformBrowser, NgForOf, NgIf } from '@angular/common';
import { Component, Inject, PLATFORM_ID } from '@angular/core';
import { RouterLink, RouterLinkActive, RouterOutlet } from '@angular/router';
import { SessionService } from '../services/authentication/session.service';
import { SpecialOfferService } from '../services/special-offer/special-offer.service';

import AppointmentReminderResponse from '../services/appointment-reminder/appointment-reminder';
import { AppointmentReminderService } from '../services/appointment-reminder/appointment-reminder.service';
import SpecialOfferResponse from '../services/special-offer/special-offer';
@Component({
  selector: 'app-topbar',
  standalone: true,
  imports: [NgIf, NgForOf, RouterOutlet, RouterLink, RouterLinkActive],
  templateUrl: './topbar.component.html',
  styleUrl: './topbar.component.css',
})
export class TopbarComponent {
  isUser: boolean = true;
  constructor(
    private specialOfferService: SpecialOfferService,
    private appointmentReminderService: AppointmentReminderService,
    @Inject(PLATFORM_ID) private platformId: Object,
    private sessionStore: SessionService
  ) {}
  userName: string = '';
  notificationArray: SpecialOfferResponse[] = [];
  closeAppointmentArray: AppointmentReminderResponse[] = [];
  ngOnInit(): void {
    if (isPlatformBrowser(this.platformId)) {
      this.getCurrentSpecialOffers();
      this.getCloseAppointments();
      if (this.sessionStore.getData('role') != 'user') {
        this.isUser = false;
      }
    }
  }

  getCurrentSpecialOffers() {
    this.specialOfferService.getCurrentSpecialOffers().subscribe({
      next: (v) => {
        if (v.body != undefined && v.body != null && v.body.length > 0) {
          for (let i = 0; i < v.body.length; i++) {
            if (
              !this.notificationArray.some(
                (item) => item._id == v.body?.[i]._id
              )
            ) {
              this.notificationArray.push(v.body?.[i]);
            }
          }
        } else {
          this.notificationArray = [];
        }
      },
    });
  }

  getCloseAppointments() {
    let userId = this.sessionStore.getData('id');
    this.appointmentReminderService
      .getCloseAppointments(userId ?? '')
      .subscribe({
        next: (v) => {
          if (v.body != undefined && v.body != null && v.body.length > 0) {
            for (let i = 0; i < v.body.length; i++) {
              if (
                !this.closeAppointmentArray.some(
                  (item) => item._id == v.body?.[i]._id
                )
              ) {
                this.closeAppointmentArray.push(v.body?.[i]);
              }
            }
          } else {
            this.closeAppointmentArray = [];
          }
        },
      });
  }
}
