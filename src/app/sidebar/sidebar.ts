export default interface Sidebar {
    menuName:string,
    icon:string,
    link:string
}