import { isPlatformBrowser, NgForOf } from '@angular/common';
import { Component, Inject, OnInit, PLATFORM_ID } from '@angular/core';
import { RouterLink, RouterLinkActive } from '@angular/router';
import { SessionService } from '../services/authentication/session.service';
import Sidebar from './sidebar';

const menuItems = {
  user: [
    {
      menuName: 'Our services',
      icon: 'fas fa-handshake',
      link: 'serviceList',
    },
    {
      menuName: 'Book an appointement',
      icon: 'fas fa-calendar-alt',
      link: 'bookAppointment',
    },
    {
      menuName: 'Your appointments',
      icon: 'fas fa-history',
      link: 'userAppointment',
    },
    {
      menuName: 'Preferences',
      icon: 'fas fa-hand-holding-heart',
      link: 'user-preference',
    },
  ],
  employee: [
    {
      menuName: 'Appointments',
      icon: 'fas fa-calendar-alt',
      link: 'employeeAppointment',
    },
    {
      menuName: 'Profile',
      icon: 'fas fa-user-alt',
      link: 'employeeProfile',
    },
  ],
  admin: [
    {
      menuName: 'Service management',
      icon: 'fas fa-cogs',
      link: 'manageService',
    },
    {
      menuName: 'Employee management',
      icon: 'fas fa-users-cogs',
      link: 'employeeProfile',
    },
    {
      menuName: 'Statistics',
      icon: 'fas fa-chart-bar',
      link: 'statistics',
    },
  ],
};

@Component({
  selector: 'app-sidebar',
  standalone: true,
  imports: [RouterLink, RouterLinkActive, NgForOf],
  templateUrl: './sidebar.component.html',
  styleUrl: './sidebar.component.css',
})
export class SidebarComponent implements OnInit {
  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    private sessionStore: SessionService
  ) {}
  menuItems: Sidebar[] = [];
  ngOnInit(): void {
    if (isPlatformBrowser(this.platformId)) {
      let currentMenu: string = 'user';
      const role = this.sessionStore.getData('role');
      if (role != undefined && role != null) {
        currentMenu = role.toString();
      }
      this.menuItems = menuItems[currentMenu as keyof typeof menuItems];
    }
  }
}
