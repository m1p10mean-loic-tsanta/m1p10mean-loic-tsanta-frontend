import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';

import { NgModule } from '@angular/core';
import { AppointmentComponent } from './appointment/appointment.component';
import { BookAppointmentComponent } from './book-appointment/book-appointment.component';

import { EmployeeAppointmentTaskComponent } from './employee-appointment-task/employee-appointment-task.component';
import { EmployeeAppointmentComponent } from './employee-appointment/employee-appointment.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ServiceComponent } from './service/service.component';
import { UserAppointmentComponent } from './user-appointment/user-appointment.component';
import { UserPreferencesComponent } from './user-preferences/user-preferences.component';

export const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent,
    title: 'home',
    children: [
      {
        path: 'appointment',
        title: 'Appointment',
        component: AppointmentComponent,
      },
      {
        path: 'bookAppointment/:targetServiceId',
        title: 'Book Appointment',
        component: BookAppointmentComponent,
      },
      {
        path: 'bookAppointment',
        title: 'Book Appointment',
        component: BookAppointmentComponent,
      },
      {
        path: 'serviceList',
        title: 'Service List',
        component: ServiceComponent,
      },
      {
        path: 'userAppointment',
        title: 'User Appointment',
        component: UserAppointmentComponent,
      },
      {
        path: 'user-preference',
        component: UserPreferencesComponent,
      },
      {
        path: 'employeeAppointment',
        component: EmployeeAppointmentComponent,
      },
      {
        path: 'employee-appointment-task/:appointmentId',
        component: EmployeeAppointmentTaskComponent,
      },
    ],
  },
  { path: '', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: 'reset-password/:id', component: ResetPasswordComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
