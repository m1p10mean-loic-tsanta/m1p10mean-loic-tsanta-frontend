import { isPlatformBrowser, NgForOf, NgIf } from '@angular/common';
import { Component, Inject, OnInit, PLATFORM_ID } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterLink, RouterLinkActive, RouterOutlet } from '@angular/router';
import Employee from '../services/employee/employee';
import { EmployeeService } from '../services/employee/employee.service';
import Service from '../services/service/service';
import { ServiceService } from '../services/service/service.service';
import UserEmployeePreference, {
  UserServicePreference,
} from '../services/user-preferences/user-preferences';
import { UserPreferencesService } from '../services/user-preferences/user-preferences.service';
import { SessionService } from './../services/authentication/session.service';
@Component({
  selector: 'app-user-preferences',
  standalone: true,
  imports: [
    NgForOf,
    NgIf,
    ReactiveFormsModule,
    RouterOutlet,
    RouterLink,
    RouterLinkActive,
  ],
  templateUrl: './user-preferences.component.html',
  styleUrl: './user-preferences.component.css',
})
export class UserPreferencesComponent implements OnInit {
  listServices: Service[] = [];
  listEmployee: Employee[] = [];
  listServicePreference: UserServicePreference[] = [];
  listEmployeePreference: UserEmployeePreference[] = [];
  constructor(
    private serviceService: ServiceService,
    private employeeService: EmployeeService,
    private userPreferenceService: UserPreferencesService,
    private sessionStore: SessionService,
    @Inject(PLATFORM_ID) private platformId: Object
  ) {}
  ngOnInit(): void {
    if (isPlatformBrowser(this.platformId)) {
      let userId = this.sessionStore.getData('id');
      this.serviceService.getDefaultServices(userId ?? '').subscribe({
        next: (v) => {
          if (v.body !== null && v.body.length > 0) {
            this.listServices = v.body;
          }
        },
        error: (e) => {
          console.log(e);
        },
      });
      this.employeeService.getAllEmployee(userId ?? '').subscribe({
        next: (v) => {
          if (v.body !== null && v.body.length > 0) {
            this.listEmployee = v.body;
          }
        },
        error: (e) => {
          console.log('error');
        },
      });
      this.userPreferenceService
        .getUserEmployeePreference(userId ?? '')
        .subscribe({
          next: (v) => {
            if (v.body !== null && v.body.length > 0) {
              this.listEmployeePreference = v.body;
            }
          },
          error: (e) => {
            console.log('error');
          },
        });
      this.userPreferenceService
        .getUserServicePreference(userId ?? '')
        .subscribe({
          next: (v) => {
            if (v.body !== null && v.body.length > 0) {
              this.listServicePreference = v.body;
            }
          },
          error: (e) => {
            console.log('error');
          },
        });
    }
  }
  deletePreference(id: string, categoryPreference: string) {
    let userId = this.sessionStore.getData('id');
    if (categoryPreference == 'service') {
      this.userPreferenceService
        .deleteUserServicePreference(id ?? '', userId ?? '')
        .subscribe({
          next: (v) => {
            if (v.body !== null) {
              this.listServicePreference = this.listServicePreference.filter(
                (item) => item.serviceId !== id
              );
            }
          },
          error: (e) => {
            console.log(e);
          },
        });
    } else {
      this.userPreferenceService
        .deleteUserEmployeePreference(id ?? '', userId ?? '')
        .subscribe({
          next: (v) => {
            if (v.body !== null) {
              this.listEmployeePreference = this.listEmployeePreference.filter(
                (item) => item.employeeId !== id
              );
            }
          },
          error: (e) => {
            console.log(e);
          },
        });
    }
  }
  addPreference(id: string, categoryPreference: string) {
    let userId = this.sessionStore.getData('id');
    let data: UserServicePreference | UserEmployeePreference;
    if (categoryPreference == 'service') {
      data = {
        userId: userId ?? '',
        serviceId: id ?? '',
      };
      this.userPreferenceService.addUserServicePreference(data).subscribe({
        next: (v) => {
          if (v.body !== null) {
            this.listServicePreference.push(data as UserServicePreference);
          }
        },
        error: (e) => {
          console.log(e);
        },
      });
    } else {
      data = {
        userId: userId ?? '',
        employeeId: id ?? '',
      };
      this.userPreferenceService.addUserEmployeePreference(data).subscribe({
        next: (v) => {
          if (v.body !== null) {
            this.listEmployeePreference.push(data as UserEmployeePreference);
          }
        },
        error: (e) => {
          console.log(e);
        },
      });
    }
  }
  isAmongPreference(id: string, categoryPreference: string) {
    if (categoryPreference == 'service') {
      return this.listServicePreference
        .map((item) => item.serviceId)
        .includes(id);
    } else {
      return this.listEmployeePreference
        .map((item) => item.employeeId)
        .includes(id);
    }
  }
}
