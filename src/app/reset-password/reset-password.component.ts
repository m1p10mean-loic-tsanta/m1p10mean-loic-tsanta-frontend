import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { Router, RouterLink, RouterLinkActive, RouterOutlet } from '@angular/router';
import { NgForOf, NgIf } from '@angular/common';
import { ForgotPasswordService } from '../services/forgot-password/forgot-password.service';
import {  resetPasswordData } from '../services/forgot-password/forgot-password';

@Component({
  selector: 'app-reset-password',
  standalone: true,
  imports: [NgForOf,NgIf,ReactiveFormsModule, RouterOutlet, RouterLink, RouterLinkActive],
  templateUrl: './reset-password.component.html',
  styleUrl: './reset-password.component.css'
})
export class ResetPasswordComponent implements OnInit{
  @Input() id!:string;
  passwordResetLoading:boolean=false
  errorWhileResetPassword:string=""
  successMessage:boolean=false
  resetForm = new FormGroup({
    password: new FormControl('',Validators.minLength(7)),
  });
  constructor(private service: ForgotPasswordService,private router: Router){
    
  }
  ngOnInit(){
    console.log(this.id)
    if(this.id==""||this.id==undefined){
      this.router.navigate([''])
    }
  }
  resetPassword(){
    this.passwordResetLoading=true
    const data={
      password:this.resetForm.value.password
    } as resetPasswordData
    if(this.resetForm.value.password!=undefined&&this.resetForm.value.password!=""){
      this.service.resetPassword(this.id,data).subscribe({
        next: (v) =>{
          this.errorWhileResetPassword=""
          this.passwordResetLoading=false
          this.successMessage=true
        },
        error: (e) =>{ 
          this.errorWhileResetPassword="Error while reseting password"
          this.passwordResetLoading=false
        },
      })
    }
    else{
      this.errorWhileResetPassword="Error while reseting password"

    }
  }
}
