import { ApplicationConfig } from '@angular/core';
import { provideRouter,withComponentInputBinding  } from '@angular/router';

import { routes } from './app.routes';
import { provideClientHydration } from '@angular/platform-browser';
import { HttpClientModule, provideHttpClient, withFetch } from '@angular/common/http';
import { importProvidersFrom } from '@angular/core';
import {AuthenticationInterceptorProvider} from './services/authentication/authentication-interceptor.service';

export const appConfig: ApplicationConfig = {
  providers: [provideRouter(routes,withComponentInputBinding()), provideClientHydration(), importProvidersFrom(HttpClientModule),provideHttpClient(withFetch()),AuthenticationInterceptorProvider]
};
